#Boilerplate para iniciar um projeto utilizando virtualenv, testes unitários e hot reload

Para elaboração deste boilerplate foi utilizado o terminal do ubuntu instalado por meio de "Adicionar recursos do Windows 10".


Este método consite em um programa que escuta as alterações nos arquivos na pasta onde se eoncontra sendo executada, e refaz os testes caa vez que alguma alteração é salva

Os testes deverão seguir o modelo do arquivo "tests/test_test.py"

Para que o arquivo seja executado ao salvar a alteração, ele deverá por padrão ter a palavra 'test' no nome do arquivo, sendo que para que o método seja executado ele deverá também obrigatoriamente que ter a palavra 'test' no nome do método, sendo que não pode repetir o nome de um método já existente.


##Versão do python: 
```
$ python3 --version                                                                                                                              
Python 3.6.7                                                                        
```


##Instalar a virtual 
```
$ sudo apt update
$ sudo apt install virtualenv
```


##Criar a virtualenv

```
$ virtualenv --python='/usr/bin/python3' .venv
$ source .venv/bin/activate 
```


##Saída
```
(.venv) marciomauricio@VSA0312:/mnt/c/Users/mauricioacm/Documents/Repos/python-boilerplate$ 
```

##Sepre que o repositório for utilizado pela primeira a virtualenv deverá ser executada 
``` 
$ source .venv/bin/activate && python --version
```

##Saída
```
Python 3.6.8
```

#Instalar as dependências deste módulo
```
pip install -r requirements.txt

```

para instalar um módulo pythoh utilize 
```
pip install mymodule
```

Logo depois que o módulo foi instalado utilize o comando a seguir para registrar o módulo no arquivo de dependências. 
```
pip freeze > requirements.txt

```

Rodando a rotina de testes

```
$ ptw
```



##A partir da segunda vez qeu você pode iniciar o projeto com os seguintes comandos
```

$ source .venv/bin/activate 
$ git pull origin master
$ pip install -r requirements.txt
$ ptw

```

